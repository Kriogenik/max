package models

import "github.com/jinzhu/gorm"

type Photo struct {
	gorm.Model
	Path   string
	PostID uint
}
