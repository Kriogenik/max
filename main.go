package main

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"

	"./db"
	"./models"
	"github.com/labstack/echo"
)

const baseURL = "https://xn--90adear.xn--p1ai/"

type State struct {
	Session string
}

func main() {
	db.Init()
	e := echo.New()
	e.Start(":3000")
	state := State{}
	GetFormPage(&state)
}

func UploadPost(c echo.Context) error {
	db := db.Manager()
	post := models.Post{}

	if err := c.Bind(post); err != nil {
		return err
	}

	db.Create(&post)

	form, err := c.MultipartForm()
	if err != nil {
		return err
	}

	files := form.File["photos"]
	for _, file := range files {
		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		photo := models.Photo{
			Path:   "./photo" + file.Filename,
			PostID: post.ID,
		}

		dst, err := os.Create(photo.Path)
		if err != nil {
			return err
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return err
		}

		db.Create(&photo)
	}

	return c.JSON(http.StatusOK, post)
}

// Получает форму, которая находится на сайте, здесь ещё где-то находится капча, ВЫТАЩИ ЕЁ ПОЛНОСТЬЮ, дальше вытаскиваем ID сессии
func GetFormPage(state *State) {
	form := url.Values{
		"agree": {"on"},
		"step":  {"2"},
	}

	client := http.Client{}

	resp, _ := client.PostForm(baseURL+"request_main/", form)
	defer resp.Body.Close()

	cookies := resp.Cookies()
	for _, cookie := range cookies {
		fmt.Println(cookie)
		if cookie.Name == "session" {
			state.Session = cookie.Value
		}
	}

	// Капча класса .img
	// body, _ := ioutil.ReadAll(resp.Body)

	// fmt.Println("response Cookies:", resp.Cookies())
	// fmt.Println("response Status:", resp.Status)
	// fmt.Println("response Headers:", resp.Header)
	// fmt.Println("response Body:", string(body))
}

func PreCheckAppeal(state *State) {
	u, _ := url.Parse(baseURL)
	cookie := http.Cookie{
		Name:  "session",
		Value: state.Session,
	}
	jar, _ := cookiejar.New(nil)
	jar.SetCookies(u, []*http.Cookie{&cookie})

	form := url.Values{
		"region_code":     {"01"},
		"subunit":         {"1"},
		"post":            {""},
		"fio":             {""},
		"is_organization": {"0"},
		"surname":         {"test"},
		"firstname":       {"test"},
		"patronymic":      {""},
		"org_name":        {""},
		"org_dop":         {""},
		"org_out":         {""},
		"org_date":        {""},
		"org_letter":      {""},
		"email":           {"test@mail.ru"},
		"phone":           {""},
		"event_region":    {""},
		"subunit_name":    {""},
		"subunit_date":    {""},
		"message":         {"test"},
		"captcha":         {"КРК7Р"},
		"step":            {"3"},
		"agree":           {"on"},
	}

	client := http.Client{
		Jar: jar,
	}

	resp, _ := client.PostForm(baseURL+"request_main/pre_check_appeal/", form)
	defer resp.Body.Close()

	// сохранить в БД эту сессию

	// Сервер вернет: {success:true}
	// body, _ := ioutil.ReadAll(resp.Body)
}

func ConfirmMail(state *State) {
	u, _ := url.Parse(baseURL)
	cookie := http.Cookie{
		Name:  "session",
		Value: state.Session,
	}
	jar, _ := cookiejar.New(nil)
	jar.SetCookies(u, []*http.Cookie{&cookie})

	form := url.Values{
		"email":      {"xatasulu@travala10.com"},
		"firstname":  {"test"},
		"patronymic": {""},
	}

	client := http.Client{
		Jar: jar,
	}

	resp, _ := client.PostForm(baseURL+"request_main/confirm_mail/", form)
	defer resp.Body.Close()

	// Сервер вернет: {success: true, message: "Письмо было отправлено Вам на почту"}
	// body, _ := ioutil.ReadAll(resp.Body)

}

func CheckCode(state *State) {
	u, _ := url.Parse(baseURL)
	cookie := http.Cookie{
		Name:  "session",
		Value: state.Session,
	}
	jar, _ := cookiejar.New(nil)
	jar.SetCookies(u, []*http.Cookie{&cookie})

	form := url.Values{
		"key":   {"РБДП7"},
		"email": {"xatasulu@travala10.com"},
	}

	client := http.Client{
		Jar: jar,
	}

	resp, _ := client.PostForm(baseURL+"request_main/check_code/", form)
	defer resp.Body.Close()

	// Сервер вернет: {success: true, message: "Почта подтверждена!"}
	// body, _ := ioutil.ReadAll(resp.Body)
}

func SendForm(state *State) {
	u, _ := url.Parse(baseURL)
	cookie := http.Cookie{
		Name:  "session",
		Value: state.Session,
	}
	jar, _ := cookiejar.New(nil)
	jar.SetCookies(u, []*http.Cookie{&cookie})

	client := http.Client{
		Jar: jar,
	}

	values := map[string]string{
		"region_code":     "01",
		"subunit":         "1",
		"post":            "",
		"fio":             "",
		"is_organization": "0",
		"surname":         "test",
		"firstname":       "test",
		"patronymic":      "",
		"org_name":        "",
		"org_dop":         "",
		"org_out":         "",
		"org_date":        "",
		"org_letter":      "",
		"email":           "test@mail.ru",
		"phone":           "",
		"event_region":    "",
		"subunit_name":    "",
		"subunit_date":    "",
		"message":         "test",
		"captcha":         "КРК7Р",
		"step":            "3",
		"agree":           "on",
	}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	for key, value := range values {
		writer.WriteField(key, value)
	}
	defer writer.Close()

	req, _ := http.NewRequest("POST", baseURL+"request_main/", body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp, _ := client.Do(req)
	defer resp.Body.Close()
}
