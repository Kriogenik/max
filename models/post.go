package models

import "github.com/jinzhu/gorm"

type Post struct {
	gorm.Model
	Pole1  string
	Pole2  string
	Pole3  string
	Photos []Photo `gorm:"FOREIGNKEY:PostID"`
}
